
function guardarDatos() {
    localStorage.producto = document.getElementById("producto").value;
    localStorage.descripcion = document.getElementById("descripcion").value;
    localStorage.unidad = document.getElementById("unidad").value;
    localStorage.cantidad = document.getElementById("cantidad").value;
    localStorage.proveedor = document.getElementById("proveedor").value;
}

function mostrarDatos() {
    if ((localStorage.producto != undefined) && (localStorage.descripcion != undefined) && (localStorage.unidad != undefined) && (localStorage.cantidad != undefined) && (localStorage.proveedor != undefined)) {
        document.getElementById("datos").innerHTML = "Producto: " + localStorage.producto + " Descripcion: " + localStorage.descripcion + " Unidad: " + localStorage.unidad + " Cantidad: " + localStorage.cantidad + " Proveedor: " + localStorage.proveedor;
    } else {
        document.getElementById("datos").innerHTML = "No has introducido ningun producto";
    }
}
